/**
 * Created by byokke on 06.12.16.
 */
const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const multer = require('multer');
const crypto = require('crypto');
const mailer = require('express-mailer');
const fs = require('fs');

let routes = require('./routes/index');
let user = require('./routes/user');
let image = require('./routes/target');
let validate = require('./routes/validate');

const app = express();
const PORT = 3000;

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

//config file
let data = fs.readFileSync(__dirname + '/../config.json', 'utf-8');
let configJson = JSON.parse(data);

//email module
mailer.extend(app, {
    from: configJson.Mailer.email,
    host: 'smtp.yandex.ru', // hostname
    secureConnection: true, // use SSL
    port: 465, // port for secure SMTP
    transportMethod: 'SMTP',
    auth: {
        user: configJson.Mailer.email,
        pass: configJson.Mailer.password
    }
});

//file loader
let storage = multer.diskStorage({
    destination: './public/images',
    filename: (req, file, cb) => {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            if (err) return cb(err);
            cb(null, raw.toString('hex') + path.extname(file.originalname))
        })
    }
});
let images = [{ name: 'target', maxCount: 1 }, { name: 'check_target', maxCount: 1 }];
app.use(multer({storage: storage}).fields(images));

//routers
app.use('/', routes);
app.use('/user', user);
app.use('/target', image);
app.use('/validate', validate);

app.use((req, res, next) => {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

if (app.get('env') === 'development') {
    app.use((err, req, res, next) => {
        res.status(err.status || 500);
        console.error(err);
        res.json({
            message: err.message,
            error: err
        });
    });
}

app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: {}
    });
});

app.listen(PORT, () => {
    console.log('Server started at port', PORT)
});