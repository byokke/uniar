let mongoose = require('mongoose');
let fs = require('fs');

try {
    let data = fs.readFileSync(__dirname + '/../../config.json', 'utf-8');
    let json = JSON.parse(data);
    if(process.env.DB_HOST == undefined) {
        process.env.DB_HOST = json["dbhost"];
        console.log('set DB_HOST value ', process.env.DB_HOST);
    }
    if(process.env.DB_PORT == undefined) {
        process.env.DB_PORT = json["dbport"];
        console.log('set DB_PORT value ', process.env.DB_PORT);
    }
    if(process.env.DB_NAME == undefined) {
        process.env.DB_NAME = json["dbname"];
        console.log('set DB_NAME value ', process.env.DB_NAME);
    }
}
catch(e) {
    console.error('Error parse config.json file', e);
}

let connectionUrl = 'mongodb://' + process.env.DB_HOST + ':' + process.env.DB_PORT + '/' + process.env.DB_NAME;

mongoose.connect(connectionUrl, function(err) {
    if(err) {
        console.error('Mongoose. Connection failure.', err)
        throw err
    }
    console.log('Mongoose successfully connected to database with name ' +
        process.env.DB_NAME + ' on ' + process.env.DB_HOST + ':' + process.env.DB_PORT)
});

module.exports = mongoose;