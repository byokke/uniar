const express = require('express');
let router = express.Router();
let User = require('../models/users').User;

router.route('/username')
    .post((req, res, next) => {
        let username = req.body.username;

        User.findOne({username: username})
            .then((result) => {
                if (!result) {
                    return res.sendStatus(200);
                } else {
                    throw new Error('User exist');
                }
            })
            .catch(err => {
                err.status = 400;
                next(err);
            })
    });

router.route('/email')
    .post((req, res, next)=> {
        let email = req.body.email;

        User.findOne({email: email})
            .then((result) => {
                if (!result) {
                    return res.sendStatus(200);
                } else {
                    throw new Error('User exist');
                }
            })
            .catch(err => {
                err.status = 400;
                next(err);
            })
    });

module.exports = router;