const express = require('express');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

let router = express.Router();
let User = require('../models/users').User;
let urltoken = require("rand-token");
let Bluebird = require('bluebird');
let dataConfig = fs.readFileSync(__dirname + '/../../config.json', 'utf-8');
let configJson = JSON.parse(dataConfig);
let jwt_sec = require('express-jwt');

router.route('/')
    .get((req, res, next) => {
        return res.send('GET /user/');
    })

    .post((req, res, next) => {
        return res.send('POST /user/');
    });

router.route('/register')
    .get((req, res, next) => {
        return res.render('index');
    })

    .post((req, res, next) => {
        let email = req.body.email;
        let username = req.body.username;
        let password = req.body.password;
        let errors = [];

        if (validateEmail(email) == false) {
            errors.push({
                message: 'Неправильный или уже существующий электронный адрес'
            });
        }

        if (password.length < 8) {
            errors.push({
                message: 'Неправильный пароль'
            });
        }

        let emailtoken = 'confirm$' + urltoken.generate(16);

        let user = new User({
            email: email,
            password: password,
            username: username,
            confirmed: false,
            payload: [{
                type: 'confirm',
                token: emailtoken
            }]
        });

        if (errors.length > 0) {
            return res.status(400).json({
                data: errors
            })
        } else {
            let sendEmailForm = {
                to: email, //
                subject: 'Email confirm',
                url: configJson.websiteURL + 'user/authentification/' + emailtoken
            };

            let sendMail = Bluebird.promisify(res.mailer.send);
            let saveUser = Bluebird.promisify(user.save);

            saveUser()
                .then(() => {
                    return sendMail("email-register", sendEmailForm);
                })
                .then(() => {
                    return res.sendStatus(200);
                })
                .catch(err => {
                    err.status = 400;
                    next(err);
                });
        }
    });

router.route('/authentification/:token')
    .get((req, res, next) => {
        let token = req.params.token;
        let type_token = token.split('$');
        User.findOne({'payload.token': token})
            .then(user => {
                if (user) {
                    if (type_token[0] == 'confirm') {
                        user.payload = [];
                        user.confirmed = true;
                        return user.save();
                    }
                    if (type_token[0] == 'newemail') {
                         // нахождение новой почты внутри массива payload
                        user.email = user.payload.find(findNewEmailInPayload).email;
                        user.payload = [];
                        return user.save()
                    }
                } else {
                    let err = new Error('User does not exist');
                    err.status = 404;
                    throw err
                }
            })
            .then(result => {
                return res.redirect('/user/login');
            })
            .catch(err => {
                next(err);
            })
    });

router.route('/login')
    .get((req, res, next) => {
        return res.render('index');
    })

    .post((req, res, next) => {
        let email = req.body.email;
        let password = req.body.password;
        let user_id;
        User.findOne({email: email})
            .then(user => {
                if (user) {
                    if (user.confirmed == true) {
                        user_id = user._id;
                        return bcrypt.compare(password, user.password)
                    } else {
                        let err = new Error('User does not confirmed');
                        err.status = 401;
                        throw err
                    }
                } else {
                    let err = new Error('User does not exist');
                    err.status = 404;
                    throw err
                }
            })
            .then(result => {
                if (result == true) {
                    let token = jwt.sign({id: user_id}, configJson.jwt_secret);
                    return res.json({
                        token: token
                    })
                } else {
                    let err = new Error('Email or password is not valid');
                    err.status = 400;
                    throw err
                }
            })
            .catch(err => {
                next(err);
            })
    });

router.route('/profile')
    .get((req, res, next) => {
        return res.render('index');
    })

    .post(jwt_sec({secret: configJson.jwt_secret}), (req, res, next) => {
        User.findById(req.user.id)
            .then(user => {
                return res.json({
                    email: user.email,
                    username: user.username
                })
            })
            .catch(err => {
                err.status = 404;
                next(err);
            })
    });

router.route('/profile/name')
    .put(jwt_sec({secret: configJson.jwt_secret}), (req, res, next) => {
        User.findById(req.user.id)
            .then(user => {
                user.username = req.body.name;
                return user.save()
            })
            .then(user => {
                return res.json({
                    email: user.email,
                    username: user.username
                })
            })
            .catch(err => {
                err.status = 404;
                next(err);
            })
    });

router.route('/profile/email')
    .put(jwt_sec({secret: configJson.jwt_secret}), (req, res, next) => {
        let email = req.body.email;
        let user;
        let emailtoken = 'newemail$' + urltoken.generate(16);
        User.count({email: email})
            .then(result => {
                if (result == 0) {
                    return User.findById(req.user.id);
                } else {
                    let error = new Error('This email is exist');
                    error.status = 400;
                    throw error;
                }
            })
            .then(_user => {
                user = _user;
                let sendEmailForm = {
                    to: email, //
                    subject: 'New email confirm',
                    url: configJson.websiteURL + 'user/authentification/' + emailtoken
                };

                let sendMail = Bluebird.promisify(res.mailer.send);

                return sendMail('email-new', sendEmailForm);
            })
            .then(() => {
                user.payload.push({
                    type: 'newemail',
                    email: email,
                    token: emailtoken
                });
                return user.save()
            })
            .then(() => {
                return res.json({});
            })
            .catch(err => {
                next(err);
            })
    });

router.route('/profile/password')
    .put(jwt_sec({secret: configJson.jwt_secret}), (req, res, next) => {
        let oldpassword = req.body.oldpassword;
        let newpassword = req.body.newpassword;

        User.findById(req.user.id)
            .then(user => {
                if (passwordHash.verify(oldpassword, user.password) == true) {
                    user.password = newpassword;
                    return user.save()
                } else {
                    let error = new Error('Password is not valid');
                    error.status = 400;
                    throw error;
                }
            })
            .then(() => {
                return res.sendStatus(200);
            })
            .catch(err => {
                next(err);
            })
    });


router.route('/forgotpassword')
    .post((req, res, next) => {
        let email = req.body.email;
        let emailtoken = 'forgotpassword$' + urltoken.generate(16);

        let sendEmailForm = {
            to: email, //
            subject: 'User forgot password',
            url: configJson.websiteURL + 'user/forgotpassword/' + emailtoken
        };

        let sendMail = Bluebird.promisify(res.mailer.send);

        sendMail("email-forgotpassword", sendEmailForm)
            .then(() => {
                return User.findOne({email: email})
            })
            .then(user => {
                if (user) {
                    user.payload.push({
                        type: 'forgotpassword',
                        token: emailtoken
                    });
                    return user.save();
                } else {
                    let error = new Error("User does not exist");
                    error.status = 404;
                    throw error;
                }
            })
            .then(() => {
                return res.sendStatus(200);
            })
            .catch(err => {
                next(err);
            })
    });

router.route('/forgotpassword/:token')
    .get((req, res, next) => {
        let token = req.params.token;
        User.findOne({'payload.token': token})
            .then(user => {
                if (user) {
                    return res.render('forgotpassword');
                } else {
                    let error = new Error('Not Found');
                    error.status = 404;
                    throw error
                }
            })
            .catch(err => {
                next(err);
            })
    })

    .post((req, res, next) => {
        let password = req.body.password;
        let password2 = req.body.password2;
        let token = req.params.token;
        if (password != password2) {
            return res.render("forgotpassword", {error: 'error'});
        } else {
            User.findOne({'payload.token': token})
                .then(user => {
                    user.password = password;
                    user.payload = [];
                    return user.save();
                })
                .then(() => {
                    return res.redirect('/user/login');
                })
                .catch(err => {
                    return res.render("forgotpassword", {error: 'error'});
                })
        }
    });

const validateEmail = (email) => {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email)
};

const findNewEmailInPayload = (item) => {
    return item.type == 'newemail';
};

module.exports = router;