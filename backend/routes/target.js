/**
 * Created by byokke on 08.12.16.
 */
const express = require('express');
const jwt_sec = require('express-jwt');

let router = express.Router();
let pastec = require("pastec")({
    server: "localhost:4212"
});
let fs = require('fs');
let Target = require('../models/target').Target;
let Bluebird = require('bluebird');
let dataConfig = fs.readFileSync(__dirname + '/../../config.json', 'utf-8');
let configJson = JSON.parse(dataConfig);

router.route('/')
    .get((req, res, next) => {
        return res.render('index');
    })

    .post(jwt_sec({secret: configJson.jwt_secret}), (req, res, next) => {
        let id = getRandomInt(1, 9999999);
        let target = new Target({
            _id: id,
            target: req.files.target[0].filename,
            title: req.body.title,
            description: req.body.description,
            userID: req.user.id
        });

        let pastecAdd = Bluebird.promisify(pastec.add);

        target.save()
            .then(() => {
                return pastecAdd(req.files.target[0].path, id);
            })
            .then((result) => {
                if (result.type == 'IMAGE_ADDED') {
                    return res.json({
                        target: req.files.target[0].filename,
                        title: req.body.title,
                        description: req.body.description
                    })
                } else {
                    target.remove();
                    return res.sendStatus(400);
                }
            })
            .catch(err => {
                deleteFile(req.files.target[0].path);
                target.remove();
                next(err);
            })
    })
    .delete(jwt_sec({secret: configJson.jwt_secret}), (req, res, next) => {
        let pastecDel = Bluebird.promisify(pastec.del);
        Target.findOneAndRemove({target: req.body.target})
            .then(target => {
                deleteFile('public/images/' + target.target);
                return pastecDel(target._id);
            })
            .then(() => {
                return res.sendStatus(200);
            })
            .catch(err => {
                next(err);
            })
    });

router.route('/list')
    .get(jwt_sec({secret: configJson.jwt_secret}), (req, res, next) => {
        Target.find({userID: req.user.id})
            .then(targets => {
                return res.json({
                    targets: targets
                })
            })
            .catch(err => {
                next(err);
            })
    });

router.route('/check')
    .post((req, res, next) => {
        pastec.fileSimilar(req.files.check_target[0].path, (err, matches, o) => {
            Bluebird.map(matches, (match) => {
                return Target.findById(match.id);
            }).then(result => {
                deleteFile(req.files.check_target[0].path);
                console.log(result);
                return res.json({
                    matches: result
                });
            }).catch(err => {
                deleteFile(req.files.check_target[0].path);
                next(err);
            });
        });
    });

function deleteFile(file) {
    return new Promise(function(resolve, reject) {
        fs.unlink(file, (err, data) => {
            if (err) reject(err);
            resolve(data);
        })
    });
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

module.exports = router;