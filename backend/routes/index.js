const express = require('express');
const jwt_sec = require('express-jwt');

let router = express.Router();
let pastec = require("pastec")({
    server: "localhost:4212"
});
let fs = require('fs');
let Target = require('../models/target').Target;
let Bluebird = require('bluebird');

router.route('/')
    .get((req, res, next) => {
        return res.render('index')
    });

router.route('/docs')
    .get((req, res, next) => {
        return res.render('index')
    });

router.route('/swift')
    .post((req, res, next) => {
        let id = getRandomInt(1, 9999999);
        let target = new Target({
            _id: id,
            target: req.files.target[0].filename,
            title: req.body.title,
            description: req.body.description,
            userID: ''
        });

        let pastecAdd = Bluebird.promisify(pastec.add);

        target.save()
            .then(() => {
                return pastecAdd(req.files.target[0].path, id);
            })
            .then((result) => {
                if (result.type == 'IMAGE_ADDED') {
                    return res.json({
                        target: req.files.target[0].filename,
                        title: req.query.title,
                        description: req.query.description
                    })
                } else {
                    target.remove();
                    return res.sendStatus(400);
                }
            })
            .catch(err => {
                deleteFile(req.files.target[0].path);
                target.remove();
                next(err);
            })
    });

function deleteFile(file) {
    return new Promise(function(resolve, reject) {
        fs.unlink(file, (err, data) => {
            if (err) reject(err);
            resolve(data);
        })
    });
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

module.exports = router;