/**
 * Created by byokke on 09.02.17.
 */
let Promise = require('bluebird');
let mongoose = Promise.promisifyAll(require('../libs/mongoose')),
    Schema = mongoose.Schema;

let schema = new Schema({
    _id: {
        type: Number,
        required: true,
        unique: true
    },
    target: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    userID: {
        type: String
    }
}, {_id: false});

let Target = mongoose.model('Target', schema);

exports.Target = Target;