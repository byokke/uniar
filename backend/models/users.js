/**
 * Created by byokke on 08.12.16.
 */
let Promise = require('bluebird');
let mongoose = Promise.promisifyAll(require('../libs/mongoose')),
    Schema = mongoose.Schema;

let bcrypt = require('bcrypt');

// схема модели пользователя
let schema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String
    },
    username: {
        type: String,
        unique: true
    },
    oauth_id: {
        type: String
    },
    confirmed: {
        type: Boolean
    },
    payload: {
        type: Array
    }
});

schema.pre('save', function(next) {
    let user = this;

    if (!user.isModified('password')) return next();
    bcrypt.hash(user.password, 10, (err, hash) => {
        user.password = hash;
        next();
    });
});

let User = mongoose.model('User', schema);

exports.User = User;