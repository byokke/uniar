import * as types from '../constants/actionTypes'
import { initialState } from './index.js'

// Редюсер для таргетов
export default function reducerTarget(state=initialState, action) {
    switch (action.type) {
        case types.ADD_TARGET_REQUEST:
            return Object.assign({}, state, {
                loading: true
            });
        case types.ADD_TARGET_RECEIVE:
            return Object.assign({}, state, {
                loading: false
            });
        case types.GET_TARGET_REQUEST:
            return Object.assign({}, state, {
                loading: true
            });
        case types.GET_TARGET_RECEIVE:
            return Object.assign({}, state, {
                loading: false
            });
        case types.DELETE_TARGET_REQUEST:
            return Object.assign({}, state, {
                loading: true
            });
        case types.DELETE_TARGET_RECEIVE:
            return Object.assign({}, state, {
                loading: false
            });
        default:
            return state;
    }
}