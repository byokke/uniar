import * as types from '../constants/actionTypes'
import { initialState } from './index.js'

// Редюсер для валидации
export default function reducerValidate(state=initialState.form, action) {
    switch (action.type) {
        case types.VALIDATE_USERNAME_REQUEST:
            return Object.assign({}, state, { form: Object.assign( {}, state, { usernameValid: false } ) });
        case types.VALIDATE_USERNAME_RECEIVE:
            return Object.assign({}, state, { form: Object.assign( {}, state, { usernameValid: action.isValid } ) });
        case types.VALIDATE_EMAIL_REQUEST:
            return Object.assign({}, state, { form: Object.assign( {}, state, { emailValid: false } ) });
        case types.VALIDATE_EMAIL_RECEIVE:
            return Object.assign({}, state, { form: Object.assign( {}, state, { emailValid: action.isValid } ) });
        default:
            return state;
    }
}