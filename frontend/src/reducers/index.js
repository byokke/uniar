/**
 * Created by byokke on 14.12.16.
 */
import { combineReducers } from 'redux'
import reducerValidate from './validate.js'
import reducerUser from './user.js'
import reducerTarget from './target.js'

export let initialState = {
    user: undefined,
    token: undefined,
    loading: false,
    fatalError: undefined,
    form: {
        loading: false,
        usernameValid: false,
        emailValid: false
    }
};

const reducers = combineReducers({
    reducerValidate,
    reducerUser,
    reducerTarget
});

export default reducers