/**
 * Created by byokke on 23.12.16.
 */
import * as types from '../constants/actionTypes'
import { initialState } from './index.js'

export default function reducerUser(state=initialState, action) {
    switch (action.type) {
        case types.SIGN_UP_REQUEST:
            return Object.assign({}, state, {
                loading: true
            });
        case types.SIGN_UP_RECEIVE:
            return Object.assign({}, state, {
                loading: false
            });
        case types.SIGN_IN_REQUEST:
            return Object.assign({}, state, {
                loading: true
            });
        case types.SIGN_IN_RECEIVE:
            return Object.assign({}, state, {
                loading: false,
                token: action.token
            });
        case types.PROFILE_REQUEST:
            return Object.assign({}, state, {
                loading: true
            });
        case types.PROFILE_RECEIVE:
            return Object.assign({}, state, {
                loading: false,
                user: action.user
            });
        case types.PROFILE_LOGOUT:
            return Object.assign({}, state, {
                user: undefined
            });
        case types.PROFILE_CHANGE_NAME_REQUEST:
            return Object.assign({}, state, {
                loading: true
            });
        case types.PROFILE_CHANGE_NAME_RECEIVE:
            if (action.user == undefined){
                return Object.assign({}, state, {
                    loading: false,
                });
            } else {
                return Object.assign({}, state, {
                    loading: false,
                    user: action.user
                });
            }
        case types.PROFILE_CHANGE_EMAIL_REQUEST:
            return Object.assign({}, state, {
                loading: true
            });
        case types.PROFILE_CHANGE_EMAIL_RECEIVE:
            return Object.assign({}, state, {
                loading: false
            });
        case types.PROFILE_CHANGE_PASSWORD_REQUEST:
            return Object.assign({}, state, {
                loading: true
            });
        case types.PROFILE_CHANGE_PASSWORD_RECEIVE:
            return Object.assign({}, state, {
                loading: false
            });
        case types.PROFILE_FORGOT_PASSWORD_REQUEST:
            return Object.assign({}, state, {
                loading: true
            });
        case types.PROFILE_FORGOT_PASSWORD_RECEIVE:
            return Object.assign({}, state, {
                loading: false
            });
        default:
            return state;
    }
}