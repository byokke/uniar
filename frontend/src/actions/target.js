import * as types from '../constants/actionTypes'
import { createRequest } from './index.js'

// Загрузка файлов на сервер
export function addTargetRequest() {
    return {
        type: types.ADD_TARGET_REQUEST
    }
}
export function addTargetReceive() {
    return {
        type: types.ADD_TARGET_RECEIVE
    }
}
export function addTargetAndImage(form, token) {
    return function(dispatch) {
        dispatch(addTargetRequest());
        return createRequest({
            url: '/target',
            method: 'post',
            body: form,
            headers: {
                'Authorization': 'bearer ' + token
            }
        }).then(
            json => {
                dispatch(addTargetReceive());
                return json
            },
            err => {
                dispatch(addTargetReceive());
                throw err
            }
        )
    }
}


// Загрузка файлов из сервера
export function getTargetRequest() {
    return {
        type: types.GET_TARGET_REQUEST
    }
}
export function getTargetReceive() {
    return {
        type: types.GET_TARGET_RECEIVE
    }
}
export function getTargetAndImage(token) {
    return function(dispatch) {
        dispatch(getTargetRequest());
        return createRequest({
            url: '/target/list',
            method: 'get',
            headers: {
                'Authorization': 'bearer ' + token
            }
        }).then(
            json => {
                dispatch(getTargetReceive());
                return json
            },
            err => {
                dispatch(getTargetReceive());
                throw err
            }
        )
    }
}


// Удалить файлы из сервера
export function deleteTargetRequest() {
    return {
        type: types.DELETE_TARGET_REQUEST
    }
}
export function deleteTargetReceive() {
    return {
        type: types.DELETE_TARGET_RECEIVE
    }
}
export function deleteTargetAndImage(token, body) {
    return function(dispatch) {
        dispatch(deleteTargetRequest());
        return createRequest({
            url: '/target',
            method: 'delete',
            body: body,
            headers: {
                'Authorization': 'bearer ' + token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(
            json => {
                dispatch(deleteTargetReceive());
                return json
            },
            err => {
                dispatch(deleteTargetReceive());
                throw err
            }
        )
    }
}