import * as types from '../constants/actionTypes'
import { createRequest } from './index.js'
import { JWT_TOKEN, JWT_REFRESH } from '../constants/keys.js'

// Регистрация
export function signUpRequest() {
    return {
        type: types.SIGN_UP_REQUEST
    }
}
export function signUpReceive() {
    return {
        type: types.SIGN_UP_RECEIVE
    }
}
export function signupUserProfile(form) {
    return function(dispatch) {
        dispatch(signUpRequest());
        return createRequest({
            url: '/user/register',
            method: 'post',
            body: form
        }).then(
            json => {
                dispatch(signUpReceive());
                return json
            },
            err => {
                dispatch(signUpReceive(undefined));
                throw err
            }
        )
    }
}


// Вход
export function signInRequest() {
    return {
        type: types.SIGN_IN_REQUEST
    }
}
export function signInReceive(token) {
    return {
        type: types.SIGN_IN_RECEIVE,
        token: token
    }
}
export function signinUserProfile(form) {
    return function(dispatch) {
        dispatch(signInRequest());
        return createRequest({
            url: '/user/login',
            method: 'post',
            body: form
        })
            .then( response => {
                return response.json()
            })
            .then(json => {
                localStorage.setItem(JWT_TOKEN, json.token);
                dispatch(signInReceive(json.token));
                return json.token;
            })
            .catch(err => {
                dispatch(signInReceive(undefined));
                throw err
            })
    }
}


// Получить профиль
export function ProfileRequest() {
    return {
        type: types.PROFILE_REQUEST
    }
}
export function ProfileReceive(user) {
    return {
        type: types.PROFILE_RECEIVE,
        user: user
    }
}
export function getUserProfile(token) {
    return function(dispatch) {
        dispatch(ProfileRequest());
        return createRequest({
            url: '/user/profile',
            method: 'post',
            headers: {
                'Authorization': 'bearer ' + token
            }
        })
            .then( response => {
                return response.json()
            })
            .then(json => {
                dispatch(ProfileReceive(json));
                return json;
            })
            .catch(err => {
                dispatch(ProfileReceive(undefined));
                throw err
            })
    }
}


//Выход из профиля
export function ProfileLogout() {
    return {
        type: types.PROFILE_LOGOUT
    }
}


//Изменение имени
export function ProfileChangeNameRequest() {
    return {
        type: types.PROFILE_CHANGE_NAME_REQUEST
    }
}
export function ProfileChangeNameReceive(user) {
    return {
        type: types.PROFILE_CHANGE_NAME_RECEIVE,
        user: user
    }
}
export function changeUserName(token, form) {
    return function(dispatch) {
        dispatch(ProfileChangeNameRequest());
        return createRequest({
            url: '/user/profile/name',
            method: 'put',
            body: form,
            headers: {
                'Authorization': 'bearer ' + token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then( response => {
                return response.json()
            })
            .then(json => {
                dispatch(ProfileChangeNameReceive(json));
                return json;
            })
            .catch(err => {
                dispatch(ProfileChangeNameReceive(undefined));
                throw err
            })
    }
}


//Изменение электронного адреса
export function ProfileChangeEmailRequest() {
    return {
        type: types.PROFILE_CHANGE_EMAIL_REQUEST
    }
}
export function ProfileChangeEmailReceive(user) {
    return {
        type: types.PROFILE_CHANGE_EMAIL_RECEIVE,
        user: user
    }
}
export function changeUserEmail(token, form) {
    return function(dispatch) {
        dispatch(ProfileChangeEmailRequest());
        return createRequest({
            url: '/user/profile/email',
            method: 'put',
            body: form,
            headers: {
                'Authorization': 'bearer ' + token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then( response => {
                return response.json()
            })
            .then(json => {
                dispatch(ProfileChangeEmailReceive(json));
                return json;
            })
            .catch(err => {
                dispatch(ProfileChangeEmailReceive(undefined));
                throw err
            })
    }
}


//Изменение пароля
export function ProfileChangePasswordRequest() {
    return {
        type: types.PROFILE_CHANGE_PASSWORD_REQUEST
    }
}
export function ProfileChangePasswordReceive() {
    return {
        type: types.PROFILE_CHANGE_PASSWORD_RECEIVE,
    }
}
export function changeUserPassword(token, form) {
    return function(dispatch) {
        dispatch(ProfileChangePasswordRequest());
        return createRequest({
            url: '/user/profile/password',
            method: 'put',
            body: form,
            headers: {
                'Authorization': 'bearer ' + token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(json => {
                dispatch(ProfileChangePasswordReceive());
                return json;
            })
            .catch(err => {
                dispatch(ProfileChangePasswordReceive());
                throw err
            })
    }
}


// Восстановление пароля
export function ProfileForgotPasswordRequest() {
    return {
        type: types.PROFILE_FORGOT_PASSWORD_REQUEST
    }
}
export function ProfileForgotPasswordReceive() {
    return {
        type: types.PROFILE_FORGOT_PASSWORD_RECEIVE,
    }
}
export function userForgotPassword(form) {
    return function(dispatch) {
        dispatch(ProfileForgotPasswordRequest());
        return createRequest({
            url: '/user/forgotpassword',
            method: 'post',
            body: form,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(json => {
                dispatch(ProfileForgotPasswordReceive());
                return json;
            })
            .catch(err => {
                dispatch(ProfileForgotPasswordReceive());
                throw err
            })
    }
}