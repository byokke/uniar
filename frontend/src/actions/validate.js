import * as types from '../constants/actionTypes'
import { createRequest } from './index.js'

//События для валидации Username
export function validateUsernameRequest() {
    return {
        type: types.VALIDATE_USERNAME_REQUEST
    }
}
export function validateUsernameReceive(isValid) {
    return {
        type: types.VALIDATE_USERNAME_RECEIVE,
        isValid: isValid
    }
}
export function validateUsername(form) {
    return (dispatch) => {
        dispatch(validateUsernameRequest())
        return createRequest({
            url: '/validate/username',
            method: 'post',
            body: form
        })
            .then(
                json => {
                    dispatch(validateUsernameReceive(true));
                    return json
                },
                err => {
                    dispatch(validateUsernameReceive(false));
                    throw err
                }
            )
    }
}

//События для валидации Email
export function validateEmailRequest() {
    return {
        type: types.VALIDATE_EMAIL_REQUEST
    }
}
export function validateEmailReceive(isValid) {
    return {
        type: types.VALIDATE_EMAIL_RECEIVE,
        isValid: isValid
    }
}
export function validateEmail(form) {
    return (dispatch) => {
        dispatch(validateEmailRequest());
        return createRequest({
            url: '/validate/email',
            method: 'post',
            body: form
        })
            .then(
                json => {
                    dispatch(validateEmailReceive(true));
                    return json
                },
                err => {
                    dispatch(validateEmailReceive(false));
                    throw err
                }
            )
    }
}

