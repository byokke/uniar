import fetch from 'isomorphic-fetch'

export const createRequest = (options) => {
    if(options.method == undefined) {
        options.method = 'get'
    }
    if(options.headers == undefined) {
        options.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }
    let url = location.origin;
    return fetch(url + options.url, options)
        .then(response => {
            if(response.status >= 400) {
                let error = new Error('Bad response from server. Response: ' + response.statusText);
                error.response = response;
                throw error
            }
            return response
        })
        .catch((err) => {
            throw err
        })
};

export * from './errors'
export * from './users'
export * from './validate'
export * from './target'

