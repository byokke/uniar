//stylus components
import '../stylus/index.styl';

//redux
import configureStore from './store';
import { Provider } from 'react-redux';

//react
import React from 'react';
import { render } from 'react-dom';
import { Router, IndexRedirect, IndexRoute, Route, Redirect, browserHistory } from 'react-router';

const store = configureStore();

//react components
import App from './app.jsx';
import Main from './containers/main.jsx';
import About from './containers/about.jsx';
import Docs from './containers/docs.jsx';
import User from './containers/user/user.jsx';
import Register from './containers/user/register.jsx';
import Login from './containers/user/login.jsx';
import Profile from './containers/user/profile.jsx';
import TargetList from './containers/target/target.jsx';

render((
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route component={App}>
                <IndexRedirect to='/'/>
                <Route path='/' component={Main} />
                <Route path='user' component={User}>
                    <Route path='register' component={Register}/>
                    <Route path='login' component={Login}/>
                    <Route path='profile' component={Profile}/>
                </Route>
                <Route path='/docs' component={Docs} />
                <Route path='/about' component={About} />
                <Route path='/target' component={TargetList} />
            </Route>
        </Router>
    </Provider>),
    document.getElementById("root")
);

