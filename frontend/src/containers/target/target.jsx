import React, { Component, PropTypes } from 'react'
import DocumentTitle from 'react-document-title'
import { JWT_TOKEN } from '../../constants/keys.js'
import ActionDialog from '../../components/dialog/action-dialog.jsx'
import Dropzone from 'react-dropzone'
import Loader from '../../components/loading.jsx'
import Success from '../../components/notification/success.jsx'
import Error from '../../components/notification/error.jsx'

class TargetList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            targets: [],
            visibleAdd: false,
            targetFile: {},
            success: '',
            error: '',
            title: '',
            description: ''
        }
    }

    componentWillMount() {
        let token = localStorage.getItem(JWT_TOKEN);
        if (this.context.state.reducerUser.user == undefined) {
            if (token) {
                this.context.actions.getUserProfile(token)
                    .then(result => {
                        console.log('User received success');
                    })
                    .catch(err => {
                        this.context.router.push('/user/login');
                    })
            } else {
                this.context.router.push('/user/login');
            }
        }
        this.context.actions.getTargetAndImage(token)
            .then(result => {
                return result.json();
            })
            .then(json => {
                let arr = [];
                json.targets.forEach((item) => {
                    arr.push({
                        target: item.target,
                        title: item.title,
                        description: item.description
                    })
                });
                this.setState({
                    targets: arr
                })
            })
            .catch(err => {
                console.log(err);
            })
    }

    componentDidUpdate() {
        if (this.state.success.length > 0 || this.state.error.length > 0) {
            setTimeout(() => {
                let notif = document.getElementsByName('notification');
                notif.forEach(item => {
                    item.style.animationName = 'unappearance';
                });
            }, 2000);
            setTimeout(() => {
                this.setState({
                    error: '',
                    success: ''
                });
            }, 2500)
        }
    }

    closeDialogAdd () {
        this.setState({
            visibleAdd: false
        })
    }

    openDialogAdd () {
        this.setState({
            visibleAdd: true
        })
    }

    onHandleAdd(e) {
        e.stopPropagation();
        e.preventDefault();

        let fileForm = new FormData();
        let token = localStorage.getItem(JWT_TOKEN);
        if (this.state.targetFile instanceof File) {
            fileForm.append('target', this.state.targetFile);
            fileForm.append('title', this.state.title);
            fileForm.append('description', this.state.description);

            this.context.actions.addTargetAndImage(fileForm, token)
                .then(result => {
                    return result.json()
                })
                .then(json => {
                    let targets = this.state.targets;
                    targets.push(json);
                    this.setState({
                        success: 'File uploaded successfully',
                        visibleAdd: false,
                        targets: targets
                    });
                })
                .catch(err => {
                    this.setState({
                        error: 'File could not upload',
                        visibleAdd: false
                    })
                })
        } else {
            this.setState({
                error: 'File could not upload',
                visibleAdd: false
            })
        }
    }

    onDrop(acceptedFiles, rejectedFiles) {
        let text = document.getElementById("droptext1");
        if (rejectedFiles.length == 0) {
            text.innerHTML = "Target is uploaded";
            text.style.color = 'green';
            this.onDragLeave(1);
            this.setState({
                targetFile: acceptedFiles[0]
            })
        } else {
            text.innerHTML = "Fail in uploading, check size or extension(JPEG, JPG)";
            text.style.color = 'red';
            this.onDragLeave(1);
        }
    }

    onDragEnter(zone) {
        let element = document.getElementById("zone"+zone);
        element.setAttribute("style", "background-color: limegreen; opacity: 0.8");
    }

    onDragLeave(zone) {
        let element = document.getElementById("zone"+zone);
        element.setAttribute("style", "background-color: #fefefe; opacity: 1");
    }

    onCloseNotification() {
        this.setState({
            success: '',
            error: ''
        })
    }

    onClickDeleteTarget(target) {
        if (confirm("Удалить?")) {
            let token = localStorage.getItem(JWT_TOKEN);
            this.context.actions.deleteTargetAndImage(token, JSON.stringify({
                target: target.target
            }))
                .then(response => {
                    let arr = this.state.targets.filter(item => {
                        return item.target != target.target
                    });
                    this.setState({
                        targets: arr,
                        success: 'Target was successfully deleted'
                    })
                })
                .catch(err => {
                    this.setState({
                        error: "Error! Try again please"
                    })
                })
        } else {
        }
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        })
    }

    onChangeDescription(e) {
        this.setState({
            description: e.target.value
        })
    }

    render() {
        let addForm =
            <div className="dropArea">
                <input type="text" placeholder="Title" name="title" onChange={this.onChangeTitle.bind(this)} value={this.state.title} required/>
                <input type="text" placeholder="Description" name="description" onChange={this.onChangeDescription.bind(this)} value={this.state.description} required/>
                <Dropzone id="zone1" className="dropzone" onDrop={this.onDrop.bind(this)} onDragEnter={this.onDragEnter.bind(this, 1)}
                    onDragLeave={this.onDragLeave.bind(this, 1)} multiple={false} accept="image/jpeg, image/png, image/jpg" name="target" maxSize={1000000}>
                    <div className="droptext" id="droptext1">Drop TARGET IMAGE here, or click to select files to upload</div>
                </Dropzone>
            </div>;
        let success = this.state.success.length > 0 ?
            <Success text={this.state.success} closeAction={this.onCloseNotification.bind(this)}/> :
            false;
        let error = this.state.error.length > 0 ?
            <Error text={this.state.error} closeAction={this.onCloseNotification.bind(this)}/> :
            false;
        let number = 0;
        let targets = this.state.targets.length > 0 ? this.state.targets.map((item) => {
            let targetURL = '/images/' + item.target;
            number++;
            return (
                <tr key={number}>
                    <td>{number}</td>
                    <td><img src={targetURL} width="100"/></td>
                    <td><span>{item.title}</span></td>
                    <td><span>{item.description}</span></td>
                    <td><i className="material-icons profile-icon delete-icon-target" onClick={this.onClickDeleteTarget.bind(this, item)}>delete_forever</i></td>
                </tr>
            )
        }) : false;
        return (
            <DocumentTitle title='your vision | Target'>
                <div>
                    <Loader loading={this.context.state.reducerTarget.loading}/>
                    {success}
                    {error}
                    <div className="over-card">
                        <h2>AR targets and objects</h2>
                    </div>
                    <div className="target-flex">
                        <div className="target-card">
                            { this.state.targets.length == 0 ?
                                <div>You do not have targets and objects</div> :
                                <table className="target-table">
                                    <thead>
                                    <tr>
                                        <th>Number</th>
                                        <th>Target</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    { targets }
                                    </tbody>
                                </table>
                            }
                            <button onClick={this.openDialogAdd.bind(this)} className="default-button">
                                Add
                            </button>
                        </div>

                        <ActionDialog form={addForm} title="Add target and objects " closeAction={this.closeDialogAdd.bind(this)}
                                      visible={this.state.visibleAdd} buttonText="Add" request={this.onHandleAdd.bind(this)}/>
                    </div>
                </div>
            </DocumentTitle>
        )
    }
}


TargetList.contextTypes = {
    actions: PropTypes.object,
    state: PropTypes.object,
    router: PropTypes.object
};

export default TargetList