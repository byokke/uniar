import React, { Component, PropTypes } from 'react'
import DocumentTitle from 'react-document-title'
import { JWT_TOKEN } from '../../constants/keys.js'
import ActionDialog from '../../components/dialog/action-dialog.jsx'
import Success from '../../components/notification/success.jsx'
import Error from '../../components/notification/error.jsx'
import Loader from '../../components/loading.jsx'
import ValidateInput from '../../components/validation/validateinput.jsx'

class Profile extends Component {
    constructor(props, context) {
        super(props);

        this.state = {
            name: '',
            email: '',
            oldpassword: '',
            newpassword: '',
            dialogVisibleName: false,
            dialogVisibleEmail: false,
            dialogVisiblePassword: false,
            success: '',
            error: '',
            formerror: false
        }
    }

    componentWillMount() {
        if (this.context.state.reducerUser.user == undefined) {
            let token = localStorage.getItem(JWT_TOKEN);
            if (token) {
                this.context.actions.getUserProfile(token)
                    .then(result => {
                        console.log('User received success');
                        this.setState({
                            name: result.username,
                            email: result.email
                        });
                    })
                    .catch(err => {
                        this.context.router.push('/user/login');
                    })
            } else {
                this.context.router.push('/user/login');
            }
        } else {
            this.setState({
                name: this.context.state.reducerUser.user.username,
                email: this.context.state.reducerUser.user.email
            });
        }
    }

    componentDidUpdate() {
        if (this.state.success.length > 0 || this.state.error.length > 0) {
            setTimeout(() => {
                let notif = document.getElementsByName('notification');
                notif.forEach(item => {
                    item.style.animationName = 'unappearance';
                });
            }, 2000);
            setTimeout(() => {
                this.setState({
                    error: '',
                    success: ''
                });
            }, 2500)
        }
    }


    dialogCloseAction() {
        this.setState({
            dialogVisibleName: false,
            dialogVisibleEmail: false,
            dialogVisiblePassword: false,
            name: this.context.state.reducerUser.user.username,
            email: this.context.state.reducerUser.user.email,
        })
    }

    onChangeName(event) {
        this.setState({
            name: event.target.value
        })
    }

    onChangeEmail(event) {
        this.setState({
            email: event.target.value
        })
    }

    onChangeOldPassword(event) {
        this.setState({
            oldpassword: event.target.value
        })
    }

    onChangeNewPassword(event) {
        this.setState({
            newpassword: event.target.value
        })
    }

    onClickEditName() {
        this.setState({
            dialogVisibleName: true
        })
    }

    onClickEditEmail() {
        this.setState({
            dialogVisibleEmail: true
        })
    }

    onClickEditPassword() {
        this.setState({
            dialogVisiblePassword: true
        })
    }

    onSubmitChangeNameHandle(event) {
        event.stopPropagation();
        event.preventDefault();

        let token = localStorage.getItem(JWT_TOKEN);
        this.context.actions.changeUserName(token, JSON.stringify({
           name: this.state.name
        }))
            .then(result => {
                this.setState({
                    success: 'Name was changed successfully',
                    dialogVisibleName: false
                })
            })
            .catch(err => {
                this.setState({
                    error: 'Name was not changed. Try again',
                    dialogVisibleName: false
                })
            })
    }

    onSubmitChangeEmailHandle(event) {
        event.stopPropagation();
        event.preventDefault();

        let token = localStorage.getItem(JWT_TOKEN);

        this.context.actions.changeUserEmail(token, JSON.stringify({
            email: this.state.email
        }))
            .then(json => {
                this.setState({
                    success: 'Success! Check letter in your new email',
                    error: '',
                    dialogVisibleEmail: false,
                    email: this.context.state.reducerUser.user.email
                })
            })
            .catch(err => {
                this.setState({
                    error: 'Error! Try again please',
                    success: '',
                    dialogVisibleEmail: false
                })
            })
    }

    onSubmitChangePasswordHandle(event) {
        event.stopPropagation();
        event.preventDefault();

        if (!this.state.formerror) {
            let token = localStorage.getItem(JWT_TOKEN);
            this.context.actions.changeUserPassword(token, JSON.stringify({
                oldpassword: this.state.oldpassword,
                newpassword: this.state.newpassword
            }))
                .then(result => {
                    this.setState({
                        success: 'Password was changed successfully',
                        dialogVisiblePassword: false
                    })
                })
                .catch(err => {
                    this.setState({
                        error: 'Error! Try again please',
                        dialogVisiblePassword: false
                    })
                })
        } else {
            console.log("error");
        }
    }

    // Валидация подтверждения пароля
    validateRepeatPassword(value, done) {
        if (value != this.state.newpassword) {
            done(false);
            this.setState({
                formerror: true
            })
        } else {
            done(true);
            this.setState({
                formerror: false
            })
        }
    }

    // Закрытия оповещения
    onCloseNotification() {
        this.setState({
            success: '',
            error: ''
        })
    }

    render() {
        let formName =
            <div>
                <label htmlFor="name">Name</label>
                <input type="text" id="name" name="name" onChange={this.onChangeName.bind(this)} value={this.state.name}/>
            </div>;
        let formEmail =
            <div>
                <label htmlFor="email">Email</label>
                <input type="email" id="email" name="email" onChange={this.onChangeEmail.bind(this)} value={this.state.email}/>
            </div>;
        let formPassword =
            <div>
                <label htmlFor="oldPassword">Old password</label>
                <input type="password" id="oldPassword" name="oldPassword" onChange={this.onChangeOldPassword.bind(this)}/>

                <div id="pass">
                    <label htmlFor="password">New password</label>
                    <input type="password" id="password" maxLength="32" name="password" pattern="^[_A-z0-9]{1,}$"
                           minLength="8" onChange={this.onChangeNewPassword.bind(this)} required/>
                </div>

                <label htmlFor="repeat-password">Repeat password</label>
                <ValidateInput name="repeatPassword" title="Repeat password" type="password" id="repeat-password" maxLength="32"
                               onChange={{validate: this.validateRepeatPassword.bind(this), error: "Passwords do not match"}}/>
            </div>;

        let success = this.state.success.length > 0 ?
            <Success text={this.state.success} closeAction={this.onCloseNotification.bind(this)}/> :
            false;
        let error = this.state.error.length > 0 ?
            <Error text={this.state.error} closeAction={this.onCloseNotification.bind(this)}/> :
            false;
        return (
            <DocumentTitle title='your vision | Profile'>
                <div>
                    <Loader loading={this.context.state.reducerUser.loading}/>
                    {success}
                    {error}
                    <div className="over-card">
                        <h2>User info</h2>
                    </div>
                    <div className="profile-card">
                        <component>
                            <i className="material-icons profile-icon">face</i>
                            <div style={{fontSize: '18px'}}>{ this.state.name }</div>
                            <i className="material-icons profile-icon-small editor" onClick={this.onClickEditName.bind(this)}>edit</i>
                        </component>

                        <component>
                            <i className="material-icons profile-icon">email</i>
                            <div style={{fontSize: '18px'}}>{ this.state.email }</div>
                            <i className="material-icons profile-icon-small editor" onClick={this.onClickEditEmail.bind(this)}>edit</i>
                        </component>

                        <component>
                            <i className="material-icons profile-icon">lock</i>
                            <div style={{fontSize: '60px', position: 'relative', lineHeight: '0', top: '-16px'}}>........</div>
                            <i className="material-icons profile-icon-small editor" onClick={this.onClickEditPassword.bind(this)}>edit</i>
                        </component>
                    </div>
                    <ActionDialog form={formName} title="Edit name" closeAction={this.dialogCloseAction.bind(this)}
                                  buttonText="Save" visible={this.state.dialogVisibleName} request={this.onSubmitChangeNameHandle.bind(this)}/>
                    <ActionDialog form={formEmail} title="Edit email" closeAction={this.dialogCloseAction.bind(this)}
                                  buttonText="Save" visible={this.state.dialogVisibleEmail} request={this.onSubmitChangeEmailHandle.bind(this)}/>
                    <ActionDialog form={formPassword} title="Edit password" closeAction={this.dialogCloseAction.bind(this)}
                                  buttonText="Save" visible={this.state.dialogVisiblePassword} request={this.onSubmitChangePasswordHandle.bind(this)}/>
                </div>
            </DocumentTitle>
        )
    }
}

Profile.contextTypes = {
    actions: PropTypes.object,
    state: PropTypes.object,
    router: PropTypes.object
};

export default Profile
