import React from 'react'

class User extends React.Component {
    render() {
        return (
            <main>
                { this.props.children }
            </main>
        )
    }
}

export default User
