import React, { Component, PropTypes } from 'react'
import DocumentTitle from 'react-document-title'
import { Link } from 'react-router'
import ValidateInput from '../../components/validation/validateinput.jsx'
import Loader from '../../components/loading.jsx'
import Success from '../../components/notification/success.jsx'
import Error from '../../components/notification/error.jsx'

class Register extends Component {
    constructor(props, context) {
        super(props);

        this.state = {
            email: '',
            username: '',
            password: '',
            error: false,
            notification: '',
            fail: false,
            success: false
        }
    }

    onSubmitHandle(event) {
        event.stopPropagation();
        event.preventDefault();

        if (this.state.error) {
            return false;
        } else {
            this.context.actions.signupUserProfile(JSON.stringify({
                email: this.state.email,
                username: this.state.username,
                password: this.state.password
            }))
                .then(response => {
                    this.setState({
                        success: true
                    })
                })
                .catch(err => {
                    this.setState({
                        fail: true
                    })
                })
        }
    }

    componentDidUpdate() {
        if (this.state.success.length > 0 || this.state.error.length > 0) {
            setTimeout(() => {
                let notif = document.getElementsByName('notification');
                notif.forEach(item => {
                    item.style.animationName = 'unappearance';
                });
            }, 2000);
            setTimeout(() => {
                this.setState({
                    error: '',
                    success: ''
                });
            }, 2500)
        }
    }


    onChangeEmail(event) {
        this.setState({
            email: event.target.value
        })
    }

    onChangeUsername(event) {
        let username = event.target.value;
        let classUsername = document.getElementById('name');
        if (username.length >=8) {
            classUsername.className = 'has-success';
        } else {
            classUsername.className = 'has-error';
        }
        this.setState({
            username: username
        })
    }

    onChangePassword(event) {
        let pass = event.target.value;
        let classPassword = document.getElementById('pass');
        if (pass.length >=4) {
            classPassword.className = 'has-success';
        } else {
            classPassword.className = 'has-error';
        }
        this.setState({
            password: pass
        })
    }

    // Валидация подтверждения пароля
    validateRepeatPassword(value, done) {
        if (value != this.state.password) {
            done(false);
            this.setState({
                error: true
            })
        } else {
            done(true);
            this.setState({
                error: false
            })
        }
    }

    // Валидация электронного адреса
    validateEmailOnChange(value, done) {
        done(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value))
    }

    validateEmailOnBlur(value, done) {
        this.setState({
            email: value
        });
        this.context.actions.validateEmail(JSON.stringify({
            email: value
        })).then(json => {
            done(true)
        }).catch(err => {
            done(false)
        })
    }

    // Закрытия оповещения
    onCloseNotification() {
        this.setState({
            success: false,
            fail: false
        })
    }

    render() {
        let success = this.state.success == true ?
            <Success text="Success! Check your email for verification" closeAction={this.onCloseNotification.bind(this)}/> :
            false;
        let error = this.state.fail == true?
            <Error text="Error! The failure occurred. Try again" closeAction={this.onCloseNotification.bind(this)}/> :
            false;
        return (
            <DocumentTitle title='your vision | Sign up'>
                <div>
                    <Loader loading={this.context.state.reducerUser.loading}/>
                    { success }
                    { error }
                    <div className="over-card">
                        <h2>Sign Up to uni-ar</h2>
                    </div>
                    <div className="card">
                        <form onSubmit={this.onSubmitHandle.bind(this)}>
                            <label htmlFor="email">Email</label>
                            <ValidateInput name="email" title="Email адрес" type="email"
                               onChange={ { validate: this.validateEmailOnChange.bind(this), error: 'Incorrect value of email' } }
                               onBlur={ { validate: this.validateEmailOnBlur.bind(this), error: 'User with the same email already exist' } } required={true}/>


                            <div id="name">
                                <label htmlFor="username">Name</label>
                                <input type="text" id="username" maxLength="32" name="username"
                                       minLength="4" onChange={this.onChangeUsername.bind(this)} required/>
                            </div>
                            <div id="pass">
                                <label htmlFor="password">Password</label>
                                <input type="password" id="password" maxLength="32" name="password" pattern="^[_A-z0-9]{1,}$"
                                minLength="8" onChange={this.onChangePassword.bind(this)} required/>
                            </div>

                            <label htmlFor="repeat-password">Repeat password</label>
                            <ValidateInput name="repeatPassword" title="Повторите пароль" type="password" id="repeat-password" maxLength="32"
                                           onChange={{validate: this.validateRepeatPassword.bind(this), error: "Passwords do not match"}}/>

                            <button type="submit" className="default-button">Sign Up</button>
                        </form>
                    </div>
                    <div className="login-footer">
                        Already have an account? <Link to="/user/login">Sign In to profile</Link>
                    </div>
                </div>
            </DocumentTitle>
        )
    }
}

Register.contextTypes = {
    actions: PropTypes.object,
    state: PropTypes.object,
    router: PropTypes.object
};

export default Register
