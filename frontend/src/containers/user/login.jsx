import React, { Component, PropTypes } from 'react'
import DocumentTitle from 'react-document-title'
import { Link } from 'react-router'
import Loader from '../../components/loading.jsx'
import Error from '../../components/notification/error.jsx'
import Success from '../../components/notification/success.jsx'
import ActionDialog from '../../components/dialog/action-dialog.jsx'

class Login extends Component {
    constructor(props, context) {
        super(props);

        this.state = {
            email: '',
            password: '',
            error: '',
            success: '',
            visiblePasswordDialog: false,
            forgotPasswordEmail: ''
        }
    }

    onSubmitHandle(event) {
        event.stopPropagation();
        event.preventDefault();

        this.context.actions.signinUserProfile(JSON.stringify({
            email: this.state.email,
            password: this.state.password
        }))
            .then(result => {
                this.context.router.push('/');
            })
            .catch(err => {
                if (err.response.status == 500) {
                    this.setState({
                        error: 'Error in the server'
                    })
                }
                if (err.response.status == 400) {
                    this.setState({
                        error: 'Email or password is not valid'
                    })
                }
                if (err.response.status == 401) {
                    this.setState({
                        error: 'User is not confirmed'
                    })
                }
                if (err.response.status == 404) {
                    this.setState({
                        error: 'User with this data does not exist'
                    })
                }
            })
    }

    componentDidUpdate() {
        if (this.state.success.length > 0 || this.state.error.length > 0) {
            setTimeout(() => {
                let notif = document.getElementsByName('notification');
                notif.forEach(item => {
                    item.style.animationName = 'unappearance';
                });
            }, 2000);
            setTimeout(() => {
                this.setState({
                    error: '',
                    success: ''
                });
            }, 2500)
        }
    }


    onSubmitForgotPasswordHandle(event) {
        event.stopPropagation();
        event.preventDefault();

        this.context.actions.userForgotPassword(JSON.stringify({
            email: this.state.forgotPasswordEmail
        }))
            .then(result => {
                this.setState({
                    success: 'Success! Check your email',
                    visiblePasswordDialog: false
                })
            })
            .catch(err => {
                this.setState({
                    error: 'Error! Try again please',
                    visiblePasswordDialog: false
                })
            })
    }

    onChangeEmail(event) {
        this.setState({
            email: event.target.value
        })
    }

    onChangePassword(event) {
        this.setState({
            password: event.target.value
        })
    }

    onChangePasswordForgotEmail(event) {
        this.setState({
            forgotPasswordEmail: event.target.value
        })
    }

    // Закрытия оповещения
    onCloseNotification() {
        this.setState({
            error: false,
            success: false
        })
    }

    showPasswordDialog() {
        this.setState({
            visiblePasswordDialog: true
        })
    }

    closePasswordDialog() {
        this.setState({
            visiblePasswordDialog: false
        })
    }

    render() {
        let error = this.state.error.length > 4 ?
            <Error text={this.state.error} closeAction={this.onCloseNotification.bind(this)}/> :
            false;

        let success = this.state.success.length > 4 ?
            <Success text={this.state.success} closeAction={this.onCloseNotification.bind(this)}/> :
            false;

        let passwordForm =
            <div>
                <label htmlFor="email">Email</label>
                <input type="email" id="email" name="name" onChange={this.onChangePasswordForgotEmail.bind(this)}/>
            </div>;

        return (
            <DocumentTitle title='your vision | Sign in'>
                <div>
                    <Loader loading={this.context.state.reducerUser.loading}/>
                    { error }
                    { success }
                    <div className="over-card">
                        <h2>Sign In to uni-ar</h2>
                    </div>
                    <div className="card">
                        <form onSubmit={this.onSubmitHandle.bind(this)}>
                            <label htmlFor="email">Email</label>
                            <input type="email" id="email" name="email" onChange={this.onChangeEmail.bind(this)}/>
                            <label htmlFor="password">Password</label>
                            <input type="password" id="password" name="password" onChange={this.onChangePassword.bind(this)}/>
                            <button type="submit" className="default-button">Sign In</button>
                        </form>
                    </div>
                    <div className="login-footer">
                        <div className="forgot-password"><a href="#" onClick={this.showPasswordDialog.bind(this)}>Forgot password?</a></div>
                        New to uni-ar? <Link to="/user/register">Create an account</Link>
                    </div>
                    <ActionDialog form={passwordForm} title="Forgot password" closeAction={this.closePasswordDialog.bind(this)}
                                  visible={this.state.visiblePasswordDialog} buttonText="Send"
                                  request={this.onSubmitForgotPasswordHandle.bind(this)}/>
                </div>
            </DocumentTitle>
        )
    }
}

Login.contextTypes = {
    actions: PropTypes.object,
    state: PropTypes.object,
    router: PropTypes.object
};

export default Login
