import React, { Component, PropTypes } from 'react'
import DocumentTitle from 'react-document-title'
import Header from '../components/header.jsx'
import { JWT_TOKEN } from '../constants/keys.js'

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentWillMount() {
        if (this.context.state.reducerUser.user == undefined) {
            let token = localStorage.getItem(JWT_TOKEN);
            if (token) {
                this.context.actions.getUserProfile(token)
            }
        }
    }

    render() {
        return (
            <DocumentTitle title='your vision'>
                <div>
                    <div className="main-container">
                        <div className="left">
                            <h1>add and recognize</h1>
                            <h1 style={{color: "limegreen"}}>Your Vision</h1>
                            <h1>simple image recognition service</h1>
                        </div>
                        <div className="right">
                        </div>
                    </div>
                    <div className="second-container">
                        <div className="function">
                            <i className="material-icons md-48">photo_camera</i>
                            <h2>make photo</h2>
                        </div>
                        <div className="function">
                            <i className="material-icons md-48">photo</i>
                            <h2>add image</h2>
                        </div>
                        <div className="function">
                            <i className="material-icons md-48">camera</i>
                            <h2>mark target</h2>
                        </div>
                        <div className="function">
                            <i className="material-icons md-48">satellite</i>
                            <h2>get recognize</h2>
                        </div>
                    </div>
                    <div className="footer">
                        <img src="/appStore.png" width="270"/>
                        <img src="/google.png" width="270"/>
                    </div>
                </div>
            </DocumentTitle>
        )
    }
}


Main.contextTypes = {
    actions: PropTypes.object,
    state: PropTypes.object,
    router: PropTypes.object
};

export default Main