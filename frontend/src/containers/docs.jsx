import React, { Component, PropTypes } from 'react'
import DocumentTitle from 'react-document-title'
import { JWT_TOKEN } from '../constants/keys.js'

class Docs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentWillMount() {
        if (this.context.state.reducerUser.user == undefined) {
            let token = localStorage.getItem(JWT_TOKEN);
            if (token) {
                this.context.actions.getUserProfile(token)
            }
        }
    }

    render() {
        return (
            <DocumentTitle title='your vision | Docs'>
                <div>
                </div>
            </DocumentTitle>
        )
    }
}


Docs.contextTypes = {
    actions: PropTypes.object,
    state: PropTypes.object,
    router: PropTypes.object
};

export default Docs