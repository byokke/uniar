import React, { Component, PropTypes } from 'react'
import DocumentTitle from 'react-document-title'
import Header from '../components/header.jsx'
import { JWT_TOKEN } from '../constants/keys.js'

class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentWillMount() {
        if (this.context.state.reducerUser.user == undefined) {
            let token = localStorage.getItem(JWT_TOKEN);
            if (token) {
                this.context.actions.getUserProfile(token)
            }
        }
    }


    render() {
        return (
            <DocumentTitle title='your vision | About'>
                <div>
                </div>
            </DocumentTitle>
        )
    }
}


About.contextTypes = {
    actions: PropTypes.object,
    state: PropTypes.object,
    router: PropTypes.object
};

export default About