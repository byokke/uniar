import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as actions from './actions'

import Header from './components/header.jsx'

let token;

class App extends Component {
    getChildContext() {
        return {
            actions: this.props.actions,
            state: this.props.state
        }
    }

    render() {
        return (
            <div>
                <Header user={this.props.state.reducerUser.user} actions={this.props.actions}/>
                <div>
                    { this.props.children }
                </div>
            </div>
        )
    }
}

App.propTypes = {
    state: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
};

App.childContextTypes = {
    state: PropTypes.object,
    actions: PropTypes.object
};

export default connect(
    (state) => {
        return {
            state: state
        }
    },
    (dispatch) => {
        return {
            actions: bindActionCreators(actions, dispatch)
        }
    }
)(App)
