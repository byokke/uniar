import React, { Component, PropTypes } from 'react'

export default class SimpleDialog extends Component {
    render() {
        if (this.props.visible) {
            return (
                <div id="myModal" className="modal">
                    <div className="modal-content">
                        <div className="modal-header">
                            <span className="close" onClick={this.props.closeAction}>&times;</span>
                            <h2>{ this.props.title }</h2>
                        </div>
                        <form onSubmit={this.props.request}>
                            <div className="modal-body">
                                { this.props.form }
                            </div>
                            <div className="modal-footer">
                                <button type="submit" className="default-button" id="loadbutton">{ this.props.buttonText }</button>
                                <a className="close-link" onClick={this.props.closeAction}>Close</a>
                            </div>
                        </form>
                    </div>
                </div>
            )
        } else {
            return false
        }
    }
}

SimpleDialog.propTypes = {
    form: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    closeAction: PropTypes.func.isRequired,
    visible: PropTypes.bool.isRequired,
    buttonText: PropTypes.string.isRequired,
    request: PropTypes.func.isRequired
};
