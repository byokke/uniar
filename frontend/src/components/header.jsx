import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { JWT_TOKEN } from '../constants/keys.js'

class Header extends React.Component {
    getTriggerElement(el)  {
        let isCollapse = el.getAttribute('data-collapse');
        if (isCollapse !== null) {
            return el;
        } else {
            let isParentCollapse = el.parentNode.getAttribute('data-collapse');
            return (isParentCollapse !== null) ? el.parentNode : undefined;
        }
    }

    collapseClickHandler(event) {
        let triggerEl = this.getTriggerElement(event.target);
        if (triggerEl === undefined) {
            event.preventDefault();
            return false;
        }

        let targetEl = document.querySelector(triggerEl.getAttribute('data-target'));
        if (targetEl) {
            triggerEl.classList.toggle('-active');
            targetEl.classList.toggle('-on');
        }
    }

    logoutUser() {
        localStorage.removeItem(JWT_TOKEN);
        this.props.actions.ProfileLogout();
    }

    render() {
        let user = this.props.user;
        return (
            <div className="navbar-component">
                <div className="navbar area">
                    <Link to="#" className="item -link brand">your vision</Link>

                    <nav role="navigation" id="navigation" className="list">
                        <Link className="item -link" activeClassName="-active"  to="/">Project</Link>
                        <Link className="item -link" activeClassName="-active"  to="/docs">Docs</Link>
                        { user ?
                            <div className="sub-menu-parent">
                                <Link className="drpdown-item drpdown-link">
                                    {user.username} &nbsp;
                                    <i className="material-icons drpdown-icon">keyboard_arrow_down</i>
                                </Link>
                                <ul className="sub-menu">
                                    <li>
                                        <Link to='/user/profile' className="sub-menu-item">Profile</Link>
                                    </li>
                                    <li>
                                        <Link to="/target" className="sub-menu-item">Targets</Link>
                                    </li>
                                    <li>
                                        <Link className="sub-menu-item" onClick={this.logoutUser.bind(this)} to="/user/login">Sign Out</Link>
                                    </li>
                                </ul>
                            </div> :
                            <Link className="item -link" activeClassName="-active"  to="/user/login">
                                Sign In &nbsp;
                                <i className="material-icons" style={{verticalAlign: "middle"}}>fingerprint</i>
                            </Link>
                        }
                    </nav>
                    <div data-collapse data-target="#navigation" className="toggle" onClick={this.collapseClickHandler.bind(this)}>
                        <span className="iconn"></span>
                    </div>
                </div>
            </div>
        )
    }
}

Header.propTypes = {
    actions: PropTypes.object,
    state: PropTypes.object,
    user: PropTypes.object
};

export default Header
