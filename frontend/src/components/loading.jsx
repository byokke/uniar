import React, { PropTypes } from 'react'

class Loading extends React.Component {
    render() {
        if (this.props.loading == true) {
            return (
                <div className="loader-back">
                    <div className="loader"></div>
                </div>
            )
        } else {
            return false
        }
    }
}

Loading.propTypes = {
    actions: PropTypes.object,
    state: PropTypes.object,
    loading: PropTypes.bool.isRequired
};

export default Loading