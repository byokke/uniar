import React, { Component, PropTypes } from 'react'

export default class Success extends Component {
    render() {
        return (
            <div name="notification" className="success-notification" onClick={this.props.closeAction}>
                <i className="material-icons" style={{verticalAlign: "middle"}}>check_circle</i> &nbsp;
                <span>{this.props.text}</span>
            </div>
        )
    }
}

Success.propTypes = {
    text: PropTypes.string.isRequired,
    closeAction: PropTypes.func.isRequired
};
