import React, { Component, PropTypes } from 'react'

export default class Error extends Component {
    render() {
        return (
            <div name="notification" className="error-notification" onClick={this.props.closeAction}>
                <i className="material-icons" style={{verticalAlign: "middle"}}>error</i> &nbsp;
                <span>{this.props.text}</span>
            </div>
        )
    }
}

Error.propTypes = {
    text: PropTypes.string.isRequired,
    closeAction: PropTypes.func.isRequired
};
