import React, { Component, PropTypes } from 'react'

export default class ValidateInput extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: '',
      isSuccessOnChange: true,
      isSuccessOnFocus: true,
      isSuccessOnBlur: true,
      help: false
    }
  }

  doneOnChange(isSuccess) {
    this.setState({
      isSuccessOnChange: isSuccess
    })
  }

  doneOnFocus(isSuccess) {
    this.setState({
      isSuccessOnFocus: isSuccess
    })
  }

  doneOnBlur(isSuccess) {
    this.setState({
      isSuccessOnBlur: isSuccess
    })
  }

  onChange(event) {
    this.setState({
      value: event.target.value
    });
    { this.props.onChange ? this.props.onChange.validate(event.target.value, this.doneOnChange.bind(this)) : this.setState({ isSuccessOnChange: true }) }
  }

  onFocus(event) {
    this.setState({
      value: event.target.value
    })
    { this.props.onFocus ? this.props.onFocus.validate(event.target.value, this.doneOnFocus.bind(this)) : this.setState({ isSuccessOnFocus: true }) }
  }

  onBlur(event) {
    this.setState({
      value: event.target.value
    });
    { this.props.onBlur ? this.props.onBlur.validate(event.target.value, this.doneOnBlur.bind(this)) : this.setState({ isSuccessOnBlur: true }) }
  }

  componentDidUpdate() {
    if (this.state.help) {
      window.setTimeout((function() {
        $("#note").fadeTo(800, 0).slideUp(800, (function(){
          $(this).remove();
          this.setState({
            help: false
          });
        }).bind(this));
      }).bind(this), 2500);
    }
  }

  render() {
    let message = this.props.def
    let isError = false
    let isSuccess = false
    if(this.state.value.length > 0) {
      if(this.state.isSuccessOnChange && this.state.isSuccessOnFocus && this.state.isSuccessOnBlur) {
        isSuccess = true
        message = this.props.success
      }
      else {
        isError = true
        message = undefined
        if(!this.state.isSuccessOnChange && this.props.onChange.error) {
          message = this.props.onChange.error
        }
        if(!this.state.isSuccessOnFocus && this.props.onFocus.error) {
          message = this.props.onFocus.error
        }
        if(!this.state.isSuccessOnBlur && this.props.onBlur.error) {
          message = this.props.onBlur.error
        }
      }
    }

    return (
      <div className={ !isError && !isSuccess ? "" : isSuccess ? "has-success" : "has-error" }>
        <input type={this.props.type} id={this.props.name} placeholder={this.props.placeholder}
           name={this.props.name} onChange={this.onChange.bind(this)}
               onFocus={this.onFocus.bind(this)} onBlur={this.onBlur.bind(this)} required={this.props.required}/>
          { isError ? <div className="error-text"> {message} </div> : null }
      </div>
    )
  }
}

ValidateInput.propTypes = {
  name: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  def: PropTypes.string,
  success: PropTypes.string,
  onChange: PropTypes.shape({
    validate: PropTypes.func.isRequired,
    error: PropTypes.string
  }),
  onFocus: PropTypes.shape({
    validate: PropTypes.func.isRequired,
    error: PropTypes.string
  }),
  onBlur: PropTypes.shape({
    validate: PropTypes.func.isRequired,
    error: PropTypes.string
  })
};

ValidateInput.defaultProps = {
  type: 'text',
  required: false
};
