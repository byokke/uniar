/**
 * Created by byokke on 14.12.16.
 */
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

const NODE_ENV = (process.env.NODE_ENV || 'development');

var OUTPUT_PATH = __dirname + '/../public/';

module.exports = {
    entry: __dirname + '/src/index.jsx',
    watch: NODE_ENV == 'development',
    debug: NODE_ENV == 'development',
    devtool: NODE_ENV == 'development' ? 'inline-cheap-source-map': null,
    output: {
        path: OUTPUT_PATH,
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            { test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader?presets[]=es2015&presets[]=react' },
            { test: /\.styl$/, exclude: /node_modules/, loader: ExtractTextPlugin.extract('style', 'css!stylus') },
            { test: /\.(ttf|eot|svg|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
            { test: /\.(jpg|png|gif|svg)$/i,
                loaders: [
                    'file?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            },
        ]
    },
    plugins: [
        new webpack.NoErrorsPlugin(),
        new ExtractTextPlugin('bundle.css'),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(NODE_ENV)
            }
        })
    ]
};

if(NODE_ENV == 'production') {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    )
}
